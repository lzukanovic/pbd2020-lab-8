package si.uni_lj.fri.pbd.lab8;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ProductDao {

    // For inserting a Product in the database add:
    @Insert
    void insertProduct(Products product);

    // For finding a product with a specific name in a database:
    @Query("SELECT * FROM products WHERE productName = :name")
    List<Products> findProduct(String name);

    // For deleting a product with a specific name from the database:
    @Query("DELETE FROM products WHERE productName = :name")
    void deleteProduct(String name);

    // For returning a LiveData object with a list of all products in the database:
    @Query("SELECT * FROM products")
    LiveData<List<Products>> getAllProducts();


}
