package si.uni_lj.fri.pbd.lab8;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Products.class}, version=1)
public abstract class ProductRoomDatabase extends RoomDatabase {

    /**
    *   static reference to this database
    */
    private static ProductRoomDatabase INSTANCE;

    /**
     *   abstract function to provide DAO for modifying the database
     */
    public abstract ProductDao productDao();

    /**
     *   executor for accessing the database in the background
     *   used for queries
     */
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    /**
     *   access method that returns a reference to the database instance
     */
    static ProductRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ProductRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ProductRoomDatabase.class,
                                    "product_database").build();
                }
            }
        }
        return INSTANCE;
    }


}
