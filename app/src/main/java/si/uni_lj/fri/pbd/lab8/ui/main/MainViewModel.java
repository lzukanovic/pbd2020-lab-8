package si.uni_lj.fri.pbd.lab8.ui.main;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.lab8.ProductRepository;
import si.uni_lj.fri.pbd.lab8.Products;

public class MainViewModel extends AndroidViewModel {

    // a list of all Products that will be shown in the RecyclerView
    private LiveData<List<Products>> allProducts;

    // details about the product that we search for
    private MutableLiveData<List<Products>> searchResults;

    private ProductRepository productRepository;

    public MainViewModel (Application application) {
        super(application);

        // set repository, allProducts, and searchResults
        productRepository = new ProductRepository(application);
        allProducts = productRepository.getAllProducts();
        searchResults = productRepository.getSearchResults();
    }


    MutableLiveData<List<Products>> getSearchResults() {
        return searchResults;
    }

    LiveData<List<Products>> getAllProducts() {
        return allProducts;
    }


    public void insertProduct(Products product) {
        productRepository.insertProduct(product);
    }
    public void findProduct(String name) {
        productRepository.findProduct(name);
    }
    public void deleteProduct(String name) {
        productRepository.deleteProduct(name);
    }

}
